package de.fredlahde.timetracker.extensions

import android.view.View
import android.widget.TextView
import org.jetbrains.anko.dip
import java.text.DateFormat
import java.util.*

fun View.dipFloat(value: Int) = dip(value).toFloat()

fun TextView.setDateFormatted(date: Date) {
    text = DateFormat.getDateTimeInstance().format(date)
}