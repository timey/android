package de.fredlahde.timetracker

import android.app.Activity
import android.os.Bundle
import android.view.View.generateViewId
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import de.fredlahde.timetracker.extensions.*
import de.fredlahde.timetracker.time.TimeTracker
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

const val FONT_SIZE = 6


class MainActivity : Activity() {
    private lateinit var txtStartDate: TextView
    private lateinit var txtEndDate: TextView
    private lateinit var txtSpendTime: TextView

    private var running = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        defaultPrefs(this)["upload_url"] = "http://timeyBackend.dev/api/times"
        setContentView(buildLayout())
    }


    private fun handleTrackingButtonClicked(v: Button) {
        running = running.toggleWithActions(v, this::handleTrackingStart, this::handleTrackingEnds)
    }


    private fun handleTrackingStart(v: Button) {
        v.text = getString(R.string.end_btn)
        val start = TimeTracker.startTracking()
        runOnUiThread {
            txtStartDate.setDateFormatted(start)
        }
    }

    private fun handleTrackingEnds(v: Button) {
        v.text = getString(R.string.start_btn)
        val end = TimeTracker.endTracking(context = this, callback = this::handleTimeSaved)
        runOnUiThread {
            txtEndDate.setDateFormatted(end)
            txtSpendTime.text = (TimeTracker.spendedTime / 1000 / 60).toString()
        }
    }

    private fun handleTimeSaved() {
        runOnUiThread {
            Toast.makeText(
                    applicationContext,
                    "Time saved!",
                    Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun buildLayout() = relativeLayout {
        val wrapperTop = linearLayout {
            id = generateViewId()
            textView {
                textSize = dipFloat(FONT_SIZE)
                id = generateViewId()
                text = "started at:"
            }.lparams {
                topMargin = dip(10)
                rightMargin = dip(4)
            }

            txtStartDate = textView {
                id = generateViewId()
                textSize = dipFloat(FONT_SIZE)
            }.lparams {
                topMargin = dip(10)
            }
        }.lparams {
            centerHorizontally()
        }

        val wrapperBottom = linearLayout {
            id = generateViewId()
            textView {
                textSize = dipFloat(FONT_SIZE)
                id = generateViewId()
                text = "ended at:"
            }.lparams {
                topMargin = dip(10)
                rightMargin = dip(4)
            }

            txtEndDate = textView {
                id = generateViewId()
                textSize = dipFloat(FONT_SIZE)
            }.lparams {
                topMargin = dip(10)
            }
        }.lparams {
            centerHorizontally()
            below(wrapperTop)
        }

        val button = button {
            id = generateViewId()
            text = getString(R.string.start_btn)
            onClick { handleTrackingButtonClicked(it as Button) }
            textSize = dipFloat(FONT_SIZE)
        }.lparams {
            below(wrapperBottom)
            topMargin = dip(10)
            centerHorizontally()
        }

        linearLayout {
            id = generateViewId()
            textView {
                textSize = dipFloat(FONT_SIZE)
                id = generateViewId()
                text = "Total Time:"
            }.lparams {
                topMargin = dip(10)
                rightMargin = dip(4)
            }

            txtSpendTime = textView {
                id = generateViewId()
                textSize = dipFloat(FONT_SIZE)
            }.lparams {
                topMargin = dip(10)
            }

        }.lparams {
            centerHorizontally()
            below(button)
        }
    }

    inline fun <T> Boolean.toggleWithActions(
            data: T,
            actionForTrue: (T) -> Unit,
            actionForFalse: (T) -> Unit
    ): Boolean {
        when (this) {
            false -> actionForFalse(data)
            true -> actionForTrue(data)
        }
        return !this
    }
}