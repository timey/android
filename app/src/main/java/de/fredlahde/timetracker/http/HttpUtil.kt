package de.fredlahde.timetracker.http

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import de.fredlahde.timetracker.extensions.defaultPrefs
import de.fredlahde.timetracker.extensions.get
import de.fredlahde.timetracker.http.eintities.Time
import de.fredlahde.timetracker.time.CallBack
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

typealias Callback = () -> Unit

object HttpUtil {
    suspend fun uploadTime(context: Context, time: Time, callback: CallBack): Response? {
        time.desc.let { Log.d(TAG, "uploading $it") }
        val url: String = defaultPrefs(context)["upload_url"] ?: throw IllegalArgumentException()
        val response = async(CommonPool) {
            return@async postTo(url, gson.toJson(time))
        }.await()
        callback()
        return response
    }

    private fun postTo(url: String, json: String, headers: Map<String, String> = emptyMap()): Response? {
        var conn: HttpURLConnection? = null
        try {
            conn = (URL(url).openConnection() as HttpURLConnection).apply {
                doInput = true
                doOutput = true
                requestMethod = "POST"
                headers.entries.forEach { setRequestProperty(it.key, it.value) }
            }

            BufferedWriter(OutputStreamWriter(conn.outputStream)).use { writer ->
                writer.write(json)
                writer.flush()
            }

            val respCode = conn.responseCode
            val respBody = StringBuilder()
            BufferedReader(InputStreamReader(conn.inputStream)).use { reader ->
                var line: String? = reader.readLine()

                while (line != null) {
                    respBody.append(line)
                    line = reader.readLine()
                }
            }

            return Response(code = respCode, body = respBody.toString())
        } finally {
            conn?.disconnect()
        }
    }

    private val TAG: String = this::class.java.simpleName
    private val gson = Gson()
}

data class Response(val code: Int, val body: String)
