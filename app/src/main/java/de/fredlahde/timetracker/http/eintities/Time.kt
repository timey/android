package de.fredlahde.timetracker.http.eintities

import java.util.*

/**
 * Created by fr3d63 on 07.07.17.
 */
data class Time(val start: Date, val end: Date, val desc: String)