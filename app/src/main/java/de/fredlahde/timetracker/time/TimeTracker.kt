package de.fredlahde.timetracker.time

import android.content.Context
import de.fredlahde.timetracker.http.HttpUtil
import de.fredlahde.timetracker.http.eintities.Time
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import java.util.*

typealias CallBack = () -> Unit

object TimeTracker {
    private lateinit var start: Date
    private lateinit var end: Date

    val spendedTime: Long
        get() = end.time - start.time

    fun startTracking(): Date {
        start = Date()
        return start
    }

    fun endTracking(context: Context, callback: CallBack): Date {
        end = Date()
        launch(CommonPool) {
            val res = HttpUtil.uploadTime(context, Time(start, end, "test"), callback)
        }
        return end
    }
}