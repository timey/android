package de.fredlahde.timetracker

import com.google.gson.Gson
import de.fredlahde.timetracker.http.HttpUtil
import de.fredlahde.timetracker.http.Response
import de.fredlahde.timetracker.http.eintities.Time
import org.junit.Assert
import org.junit.Test
import java.util.*

/**
 * Created by fr3d63 on 07.07.17.
 */
class HttpUtilTest {
    @Test
    fun it_posts_correctly() {
        val url = "https://httpbin.org/post"

        val time = Time(
                Date(),
                Date(),
                "hello"
        )
        val resp: Response? = HttpUtil.postTo(url, Gson().toJson(time))

        resp ?: Assert.fail("Response is null")

        Assert.assertEquals(200, resp?.code)

        println(resp?.body)
    }
}